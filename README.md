# DIM wishlists

This project automatically filters DIM's default [`voltron.txt`](https://github.com/48klocs/dim-wish-list-sources/blob/master/voltron.txt)
to leave only perks in 3rd and 4th columns.

It runs weekly, so there is a delay for changes to `voltron.txt` to make it
here.

Here's the URL: https://imax9000.gitlab.io/dim-wishlists/voltron.txt

NOTE: DIM currently doesn't have gitlab.io in the whitelist of allowed domains,
so it's not useable just yet.
